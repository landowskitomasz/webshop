package tech.codelane.webshop;


import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

class PriceCalculatorTest {

    @Test
    void should_calculatePrice() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.id = "1";

        Photo photo1 = new Photo("zdjecie123.jpg");
        photo1.added_at = new Date(1640790057995L);

        Photo photo2 = new Photo("zdjecie443.jpg");
        photo2.added_at = new Date(1640790057995L);

        Photo photo3 = new Photo("zdjecie653.jpg");
        photo3.added_at = new Date(1640790057999L);

        Photo photo4 = new Photo("zdjecie3.jpg");
        photo4.added_at = new Date(1640790057981L);

        shoppingCart.photos = new HashSet<>();
        shoppingCart.photos.add(photo1);
        shoppingCart.photos.add(photo2);
        shoppingCart.photos.add(photo3);
        shoppingCart.photos.add(photo4);

        Pricing pricing = new Pricing();
        pricing.photosPackage = new Pricing.PhotosPackage();
        pricing.photosPackage.price = 300;
        pricing.photosPackage.photosNumberInPackage = 3;
        pricing.priceForPhotoOverPackage = 10;

        double price = PriceCalculator.aCalculator(pricing).calculatePriceOf(shoppingCart);

        assert price == 310.0;
    }

    @Test
    void should_test_list() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.id = "1";

        Photo photo1 = new Photo("zdjecie123.jpg");
        photo1.added_at = new Date(1640790057995L);

        Photo photo2 = new Photo("zdjecie443.jpg");
        photo2.added_at = new Date(1640790057995L);

        Photo photo3 = new Photo("zdjecie653.jpg");
        photo3.added_at = new Date(1640790057999L);

        Photo photo4 = new Photo("zdjecie3.jpg");
        photo4.added_at = new Date(1640790057981L);

        shoppingCart.photos = new HashSet<>();
        shoppingCart.photos.add(photo1);
        shoppingCart.photos.add(photo2);
        shoppingCart.photos.add(photo3);
        shoppingCart.photos.add(photo4);

        Pricing pricing = new Pricing();
        pricing.photosPackage = new Pricing.PhotosPackage();
        pricing.photosPackage.price = 300;
        pricing.photosPackage.photosNumberInPackage = 3;
        pricing.priceForPhotoOverPackage = 10;

        HashMap<String, Double> priceList = PriceCalculator.aCalculator(pricing).getPriceList(shoppingCart);

        assert priceList != null;
    }

}