package tech.codelane.webshop;

public class Pricing {

    public PhotosPackage photosPackage;
    public double priceForPhotoOverPackage;

    public static class PhotosPackage {
        public double price;
        public short photosNumberInPackage;
    }
}
