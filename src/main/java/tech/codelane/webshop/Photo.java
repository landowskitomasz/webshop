package tech.codelane.webshop;

import java.util.Date;
import java.util.Objects;

public class Photo {

    private final String name;
    public Date added_at;

    public Photo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Photo photo = (Photo) o;
        return Objects.equals(added_at, photo.added_at);
    }

    @Override
    public int hashCode() {
        return (int)(added_at.getTime() % 2L);
    }
}
