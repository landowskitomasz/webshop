package tech.codelane.webshop;


import java.util.HashSet;

public class ShoppingCart {

    public String id;

    public HashSet<Photo> photos;

    public ShoppingCart() {
    }

    public ShoppingCart(String id, HashSet<Photo> photos) {
        this.id = id;
        this.photos = photos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public HashSet<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(HashSet<Photo> photos) {
        this.photos = photos;
    }
}
